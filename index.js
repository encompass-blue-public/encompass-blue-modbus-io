let integerIsByte = function(integer) {
    if (
        0 <= integer &&
        integer <= 255
    ) {
        return true;
    } else {
        return false;
    }
}

let integerIsTwoByte = function(integer) {
    if (
        0 <= integer &&
        integer <= 65535
    ) {
        return true;
    } else {
        return false;
    }
}

let integerIsBit = function(integer) {
    if (
        integer === 0 ||
        integer == 1
    ) {
        return true;
    } else {
        return false;
    }
}

let isBase64 = function(string) {
    try {
        return Buffer.from(string, 'base64').toString('base64') === string
    } catch (err) {
        return false;
    }

}

let validateModbusAddress = function(modbusAddress) {

    modbusAddress = parseInt(modbusAddress);
    if (!integerIsByte(modbusAddress)) {
        throw `modbusAddress only supports addresses 0 - 255`;
    }
    return modbusAddress;

}

let validateModbusChannel = function(modbusChannel) {

    modbusChannel = parseInt(modbusChannel);
    if (!integerIsTwoByte(modbusChannel)) {
        throw `modbusChannel only supports addresses 0 - 65535`;
    }
    return modbusChannel;
}

let validateModbusCount = function(modbusCount) {

    modbusCount = parseInt(modbusCount);
    if (!integerIsTwoByte(modbusCount)) {
        throw `modbusCount only supports addresses 0 - 65535`;
    }
    return modbusCount;
}

let validateModbusFunction = function(modbusFunction) {

    modbusFunction = parseInt(modbusFunction);
    if (!(
        modbusFunction == 1 ||
        modbusFunction == 2 ||
        modbusFunction == 3 ||
        modbusFunction == 4 ||
        modbusFunction == 5 ||
        modbusFunction == 16
    )) {
        throw `Modbus Function not currently supported: ${modbusFunction}`;
    }
    return modbusFunction;
}

let validateModbusValuesAreTwoByte = function(modbusValues) {

    if (!Array.isArray(modbusValues)) {
        throw `modbusValues should be an array`;
    }

    modbusValues.map(function(modbusValue) {
        modbusValue = parseInt(modbusValue);
        if (!integerIsTwoByte(modbusValue)) {
            throw `Values in modbusArray should be between 0 and 65535. Got:  ${modbusValue}`;
        }
        return modbusValue;
    })
    return modbusValues;

}

let validateModbusValuesAreByte = function(modbusValues) {

    if (!Array.isArray(modbusValues)) {
        throw `modbusValues should be an array`;
    }

    modbusValues.map(function(modbusValue) {
        modbusValue = parseInt(modbusValue);
        if (!integerIsByte(modbusValue)) {
            throw `Values in modbusArray should be between 0 and 255. Got:  ${modbusValue}`;
        }
        return modbusValue;
    })
    return modbusValues;

}

let validateModbusValuesAreBit = function(modbusValues) {

    if (!Array.isArray(modbusValues)) {
        throw `modbusValues should be an array`;
    }

    modbusValues.forEach(function(modbusValue) {
        if (!integerIsBit(modbusValue)) {
            throw `Value in modbusArray is not 1 or 0. Got: ${modbusValue}`;
        }
    })
    return modbusValues;

}

let validateModbusValuesLengthSingle = function(modbusValues) {

    if (modbusValues.length != 1) {
        throw `modbusValues length should be 1. Received: ${modbusValues.length}`;
    }
    return modbusValues;
}

let validateModbusValuesMatchCount = function(modbusCount, modbusValues) {
    if (modbusValues.length != modbusCount) {
        throw `modbusValues length should match modbusCount. Values Length: ${modbusValues.length} | Count: ${modbusCount}`;
    }
    return modbusValues;
}

let constructReadCoils = function(modbusAddress, modbusChannel, modbusCount) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(modbusCount);

    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([1]),
        channelBuf,
        countBuf
    ])
}

let constructReadDiscreteInputs = function(modbusAddress, modbusChannel, modbusCount) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(modbusCount);

    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([2]),
        channelBuf,
        countBuf
    ])
}

let constructReadHoldingRegisters = function(modbusAddress, modbusChannel, modbusCount) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(modbusCount);

    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([3]),
        channelBuf,
        countBuf
    ])
}

let constructReadInputRegisters = function(modbusAddress, modbusChannel, modbusCount) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(modbusCount);

    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([4]),
        channelBuf,
        countBuf
    ])
}

let constructWriteSingleCoil = function(modbusAddress, modbusChannel, modbusValues) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(1);

    const valueBuf = Buffer.alloc(2);
    valueBuf.writeUInt16BE(modbusValues[0]);

    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([5]),
        channelBuf,
        countBuf,
        valueBuf
    ])
}

let constructWriteMultipleRegisters = function(modbusAddress, modbusChannel, modbusCount, modbusValues) {

    const channelBuf = Buffer.alloc(2);
    channelBuf.writeUInt16BE(modbusChannel);

    const countBuf = Buffer.alloc(2);
    countBuf.writeUInt16BE(modbusCount);

    valuesBuf = modbusValues.map(function(modbusValue) {
        const valueBuf = Buffer.alloc(2);
        valueBuf.writeUInt16BE(modbusValue);
        return valueBuf;
    })


    return Buffer.concat([
        Buffer.from([modbusAddress]),
        Buffer.from([16]),
        channelBuf,
        countBuf,
        ...valuesBuf
    ])
}

let convertModbusADU = function(modbusADU) {

    // Can receive a buffer, a decimal array, or base64.
    let modbusBuffer;
    if (Array.isArray(modbusADU)) {
        modbusADU = validateModbusValuesAreByte(modbusADU);
        modbusBuffer = Buffer.from(modbusADU);
    } else if (Buffer.isBuffer(modbusADU)) {
        modbusBuffer = modbusADU;
    } else if (isBase64(modbusADU)) {
        modbusBuffer = Buffer.from(modbusADU, 'base64');
    } else {
        throw `Could not convert modbusADU`
    }

    return modbusBuffer;

}

module.exports.constructModbusADU = function(modbusAddress, modbusFunction, modbusChannel, modbusCount, modbusValues) {

    modbusFunction = validateModbusFunction(modbusFunction);

    if (modbusFunction == 1) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusCount = validateModbusCount(modbusCount);
        return constructReadCoils(modbusAddress, modbusChannel, modbusCount);
    }

    if (modbusFunction == 2) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusCount = validateModbusCount(modbusCount);
        return constructReadDiscreteInputs(modbusAddress, modbusChannel, modbusCount);
    }

    if (modbusFunction == 3) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusCount = validateModbusCount(modbusCount);
        return constructReadHoldingRegisters(modbusAddress, modbusChannel, modbusCount);
    }

    if (modbusFunction == 4) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusCount = validateModbusCount(modbusCount);
        return constructReadInputRegisters(modbusAddress, modbusChannel, modbusCount);
    }

    if (modbusFunction == 5) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusValues = validateModbusValuesLengthSingle(modbusValues);
        modbusValues = validateModbusValuesAreBit(modbusValues);
        return constructWriteSingleCoil(modbusAddress, modbusChannel, modbusValues);
    }

    if (modbusFunction == 16) {
        modbusAddress = validateModbusAddress(modbusAddress);
        modbusChannel = validateModbusChannel(modbusChannel);
        modbusCount = validateModbusCount(modbusCount);
        modbusValues = validateModbusValuesAreTwoByte(modbusValues);
        modbusValues = validateModbusValuesMatchCount(modbusCount, modbusValues);
        return constructWriteMultipleRegisters(modbusAddress, modbusChannel, modbusCount, modbusValues);
    }

}

// module.exports.constructModbusCustom = function(modbusAddress, modbusFunction, modbusChannel, modbusCount, modbusValues, customConstruct) {

// }

module.exports.parseModbusADU = function(modbusADU) {
    // Get a buffer
    return convertModbusADU(modbusADU);

}

// module.exports.parseModbusCustom = function(modbusADU) {

// }