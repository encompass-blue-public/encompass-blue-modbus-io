const ModbusIO = require('./index');

describe("ModbusIO Construct Modbus", () => {
    test("Encode a modbus packet, read coils", () => {
        expect(ModbusIO.constructModbusADU(5, 1, 45, 5, [3])).toEqual(Buffer.from([5, 1, 0, 45, 0, 5]));
    })
    test("Encode a modbus packet, read coils", () => {
        expect(ModbusIO.constructModbusADU(5, 1, 1456, 5, [3])).toEqual(Buffer.from([5, 1, 5, 176, 0, 5]));
    })

    test("Encode a modbus packet, read discrete inputs", () => {
        expect(ModbusIO.constructModbusADU(5, 2, 45, 70, [3])).toEqual(Buffer.from([5, 2, 0, 45, 0, 70]));
    })
    test("Encode a modbus packet, read discrete inputs", () => {
        expect(ModbusIO.constructModbusADU(5, 2, 65535, 70, [3])).toEqual(Buffer.from([5, 2, 255, 255, 0, 70]));
    })

    test("Encode a modbus packet, read holding registers", () => {
        expect(ModbusIO.constructModbusADU(5, 3, 1456, 1700, [3])).toEqual(Buffer.from([5, 3, 5, 176, 6, 164]));
    })
    test("Encode a modbus packet, read holding registers", () => {
        expect(ModbusIO.constructModbusADU(5, 3, 5, 170, [3])).toEqual(Buffer.from([5, 3, 0, 5, 0, 170]));
    })

    test("Encode a modbus packet, read input registers", () => {
        expect(ModbusIO.constructModbusADU(5, 4, 1700, 65535, [3])).toEqual(Buffer.from([5, 4, 6, 164, 255, 255]));
    })
    test("Encode a modbus packet, read input registers", () => {
        expect(ModbusIO.constructModbusADU(5, 4, 245, 207, [3])).toEqual(Buffer.from([5, 4, 0, 245, 0, 207]));
    })

    test("Encode a modbus packet, write single coil", () => {
        expect(ModbusIO.constructModbusADU(5, 5, 245, 1, [0])).toEqual(Buffer.from([5, 5, 0, 245, 0, 1, 0, 0]));
    })
    test("Encode a modbus packet, write single coil", () => {
        expect(ModbusIO.constructModbusADU(5, 5, 245, 1, [1])).toEqual(Buffer.from([5, 5, 0, 245, 0, 1, 0, 1]));
    })
    test("Encode a modbus packet, write single coil", () => {
        expect(ModbusIO.constructModbusADU(5, 5, 1700, 1, [1])).toEqual(Buffer.from([5, 5, 6, 164, 0, 1, 0, 1]));
    })

    test("Encode a modbus packet, write multiple registers", () => {
        expect(ModbusIO.constructModbusADU(5, 16, 1700, 3, [1700, 1, 65535])).toEqual(Buffer.from([5, 16, 6, 164, 0, 3, 6, 164, 0, 1, 255, 255]));
    })

    test("Encode a modbus packet, write multiple registers", () => {
        expect(ModbusIO.constructModbusADU(5, 16, 256, 2, [256, 1456])).toEqual(Buffer.from([5, 16, 1, 0, 0, 2, 1, 0, 5, 176]));
    })

    test("Encode a modbus packet, write multiple registers", () => {
        expect(ModbusIO.constructModbusADU(5, 16, 1, 1, [1700])).toEqual(Buffer.from([5, 16, 0, 1, 0, 1, 6, 164]));
    })
});


describe("ModbusIO Parser Modbus", () => {
    test("Decode a modbus packet, base64", () => {
        expect(ModbusIO.parseModbusADU("BAUGBw==")).toEqual(Buffer.from([4, 5, 6, 7]));
    })
    test("Decode a modbus packet, decimal array", () => {
        expect(ModbusIO.parseModbusADU([4, 5, 6, 7])).toEqual(Buffer.from([4, 5, 6, 7]));
    })
    test("Decode a modbus packet, buffer", () => {
        expect(ModbusIO.parseModbusADU(Buffer.from([4, 5, 6, 7]))).toEqual(Buffer.from([4, 5, 6, 7]));
    })
});